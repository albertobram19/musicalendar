enum AppState{
  calendar,
  tuner,
  timeKeeper,
  statistics,
  settings,
  createEvent,
  argumentsList
}