import 'dart:io';

import 'package:musicalendar/EventItem.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Database db;
class DBHelper{
  static const dbName = 'musicalendar_db';
  static const eventsTable = 'events';
  static const argsTable = 'args';

  Future<void> createEventsTable(Database db) async {
    final todoSql = '''CREATE TABLE $eventsTable (
      ID INTEGER PRIMARY KEY AUTOINCREMENT,
      Arg TEXT,
      DText TEXT,
      D INTEGER,
      Day INTEGER)''';
    await db.execute(todoSql);
  }

  Future<void> createArgsTable(Database db) async {
    final todoSql = '''CREATE TABLE $argsTable (
      ID INTEGER PRIMARY KEY AUTOINCREMENT,
      Name TEXT)''';
    await db.execute(todoSql);
  }

  Future<void> saveEvents(List<EventItem> _events) async {
    String arg = "";
    String Dtext = "";
    int d = 0;
    int datetime = 0;
    print(_events.toString());
    _events.forEach((element) async {
      if(element.isReadyToSave()) {
        arg = element.arg;
        Dtext = element.durationString;
        d = element.duration;
        datetime = element.datetime;
        await db.rawInsert(
            'INSERT INTO $eventsTable (Arg,Dtext,D,Day) VALUES("$arg","$Dtext",$d,$datetime)');
      }
    });
  }

  Future<void> saveNewArg(String newArg) async {
    await db.rawInsert('INSERT INTO $argsTable (Name) VALUES("$newArg")');
  }

  Future<void> editArgName(int id,String newName) async{
    //todo
  }

  Future<void> deleteArg(int id) async{
    await db.rawQuery("DELETE FROM $argsTable WHERE ID=$id");
  }

  Future<void> deleteEvent(int id) async{
    await db.rawQuery("DELETE FROM $eventsTable WHERE ID=$id");
  }

  Future<Map<DateTime,List>> getEvents() async {
    Map<DateTime,List> _events = {};

    final List<Map<String, dynamic>> maps = await db.query('$eventsTable');

    maps.forEach((element) {
      _events[DateTime.fromMillisecondsSinceEpoch(element['Day'])] = [element['Arg']];
    });

    print(_events.toString());

    return _events;
  }

  Future<Map<int,EventItem>> getEventsByDay(day) async {
    Map<int,EventItem> _events = {};

    final List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM $eventsTable WHERE Day=$day');

    maps.forEach((element) {
      _events[element['ID']] =
            new EventItem.fromData(element['Arg'],element['DText'],element['D'],element['Day']);
    });

    print(_events.toString());

    return _events;
  }

  Future<Map<int,String>> getArguments() async{
    Map<int,String> _args = {};

    final List<Map<String, dynamic>> maps = await db.query('$argsTable',orderBy: 'Name');

    maps.forEach((element) {
      _args[element['ID']] = element['Name'];
    });

    print(_args.toString());

    return _args;
  }

  Future<String> getDatabasePath(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);
    if (!await Directory(dirname(path)).exists()) {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }

  Future<void> initDatabase() async {
    final path = await getDatabasePath(dbName);
    db = await openDatabase(path, version: 3, onCreate: onCreate, onUpgrade: _onUpgrade);
    print(db);
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if(newVersion > 1 && (oldVersion == 1 || oldVersion == 2)){
        await db.rawQuery("DROP TABLE $argsTable");
        createArgsTable(db);
        await db.rawInsert('INSERT INTO $argsTable (Name) VALUES ("Solfeggio")');
    }
  }

  Future<void> onCreate(Database db, int version) async {
    await createEventsTable(db);
    await createArgsTable(db);
    //await db.rawInsert('INSERT INTO $eventsTable (ID, Arg,Dtext,D,Day) VALUES(0, "Boh 1","1 ora",60,"1611593883000")');
  }
}