import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:musicalendar/AppState.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:flutter_material_pickers/flutter_material_pickers.dart';

import 'DBHelper.dart';
import 'EventItem.dart';

final Map<DateTime, List> _holidays = {
  DateTime(2020, 1, 1): ['New Year\'s Day'],
  DateTime(2020, 1, 6): ['Epiphany'],
  DateTime(2020, 2, 14): ['Valentine\'s Day'],
  DateTime(2020, 4, 21): ['Easter Sunday'],
  DateTime(2020, 4, 22): ['Easter Monday'],
};

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DBHelper().initDatabase();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calendario Musica',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Calendario Musica'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _actualDay = 0;
  AppState _actualState = AppState.calendar;
  Map<DateTime, List> _events = {};
  Map<int,String> _args = {};
  CalendarController _calendarController;
  List<EventItem> _eventToCreate = [];

  final newArgEditController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
    updateEvents();
    updateArgs();
  }

  void updateEvents(){
    DBHelper().getEvents().then((value) => setState((){
      _events = value;
    }));
  }

  void updateArgs(){
    DBHelper().getArguments().then((value) => setState((){
      _args = value;
    }));
  }

  @override
  void dispose() {
    _calendarController.dispose();
    newArgEditController.dispose();
    super.dispose();
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  Widget getBodyWidget(BuildContext context) {
    switch(_actualState){
      case AppState.calendar:
        return new Center(
          child: _buildTableCalendar()
        );
        break;
      case AppState.createEvent:
        return new Center(
          child: Container(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text('Crea Eventi')
                  ],
                ),
                _buildCreationRowSet(context)
              ],
            ),
          )
        );
        break;
      case AppState.settings:
        return new Center(
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Text('Argomenti di studio'),
                onTap: (){ _goArguments();},
              )
            ]
          )
        );
        break;
      case AppState.argumentsList:
        return new Center(
            child: _getArgumentsWidget(context)
        );
        break;
      default:
        return new Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'In lavorazione',
              )
            ],
          ),
        );
        break;
    }
  }

  void _addArgument(BuildContext context){
    String newName = newArgEditController.text;
    if(newName != null){
      if(newName.length>0){
        DBHelper().saveNewArg(newName).then((value){
          //value is void
          newArgEditController.text = "";
          FocusScope.of(context).unfocus();
          _goArguments();
        });
      }
    }
  }

  Widget _getArgumentsWidget(BuildContext context){
    List<Widget> children = <Widget>[];
    children.add(new ListTile(
      title: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            width: 250,
            child: TextField(
                    controller: newArgEditController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Crea un unovo argomento...',
                    ),
                  )
          ),
          RaisedButton(
            child: Text('SALVA'),
            onPressed: (){_addArgument(context);},
          )
        ],
      )
    )
    );
    _args.forEach((key, value) {
      if(value != null) {
        children.add(new ListTile(
          title:Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(value),
              GestureDetector(
                child: Image.asset('assets/images/delete-cross.png',
                    width:20),
                onTap: (){_deleteArg(key);},
              )
            ]
          ),
          onTap:(){
            //todo
          }
        ));
      }
    });
    return ListView(
      children: children
    );
  }

  void _deleteArg(int id) async{
    DBHelper().deleteArg(id).then((value){
      _goArguments();
    });
  }

  Widget _buildCreationRowSet(BuildContext context){
    List<Widget> children = <Widget>[

    ];
    int i = 0;
    if(_eventToCreate.length<5){
      for(i = _eventToCreate.length;i<5;i++){
        _eventToCreate.add(new EventItem());
        children.add(_getCreateEventWidget(context, null, i));
      }
    }else if(_eventToCreate.length>0){
      for(i = 0;i<_eventToCreate.length;i++){
          children.add(_getCreateEventWidget(context,_eventToCreate[i],i));
      }
    }
    children.add(RaisedButton(
      padding: const EdgeInsets.all(8.0),
      textColor: Colors.white,
      color: Colors.blue,
      onPressed: _saveCreatedEvents,
      child: new Text("SALVA"),
    ),);
    return Column(
      children: children
    );
  }

  void _saveCreatedEvents() async{
    DBHelper().saveEvents(_eventToCreate).then((value)=>
        _goCalendar()
    );
  }

  Widget _getArgSelectorItem(EventItem event){
    if(event != null) {
      if (event.arg.length > 0) {
        return Text(event.arg);
      } else {
        return Text('Scegli argomento');
      }
    }else{
      return Text('Scegli argomento');
    }
  }

  Widget _getDurationSelectorItem(EventItem event){
    if(event != null) {
      if (event.duration > 0) {
        return Text(event.durationString);
      } else {
        return Text('Scegli durata');
      }
    }else{
      return Text('Scegli durata');
    }
  }

  Widget _getCreateEventWidget(BuildContext context,EventItem event, int index){
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          GestureDetector(
            onTap: () {
                _showArgPicker(context,event,index);
              },
              child: _getArgSelectorItem(event),
          ),
          GestureDetector(
              onTap: () {
                _showDurationPicker(context,event,index);
              },
              child: _getDurationSelectorItem(event),
          ),
        ],
      ),
    );
  }

  void _showDurationPicker(BuildContext context,EventItem event,int index) {
    String selected = '15 minuti';
    if(event != null){
      selected = event.durationString;
    }
    showMaterialScrollPicker(
      context: context,
      title: "Seleziona argomento",
      items: ['15 minuti','30 minuti','45 minuti','1 ora','1.5 ore','2 ore'],
      selectedItem: selected,
      onChanged: (value) =>
      {
        setState(() => {
          _eventToCreate[index].setDuration(value)
        })
      },
    );
  }

  void _showArgPicker(BuildContext context,EventItem event,int index) {
    String selected = '';
    if(event != null){
      selected = event.arg;
    }
    List<String> args = [];
    _args.forEach((key, value) {
      args.add(value);
    });
    showMaterialScrollPicker(
      context: context,
      title: "Seleziona argomento",
      items: args,
      selectedItem: selected,
      onChanged: (value) =>
      {
        setState(() => {
          _eventToCreate[index].datetime = _actualDay,
          _eventToCreate[index].arg = value,
        })
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return WillPopScope(
        onWillPop: () async {
          _goCalendar();
          return false;
        },
        child:Scaffold(
          appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text(widget.title),
          ),
          body: getBodyWidget(context),
          bottomNavigationBar: new Container(
              padding: EdgeInsets.all(0.0),
              child:Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    GestureDetector(
                      onTap: _goTimeKeeper,
                      child: Image.asset('assets/images/time-keeper-icon.png',
                          width:50),
                    ),
                    GestureDetector(
                      onTap: _goTuner,
                      child: Image.asset('assets/images/tuner-icon.png',
                          width:50),
                    ),
                    GestureDetector(
                      onTap: _goCalendar,
                      child: Image.asset('assets/images/calendar-icon.png',
                          width:50),
                    ),
                    GestureDetector(
                      onTap: _goStats,
                      child: Image.asset('assets/images/stats-icon.png',
                          width:50),
                    ),
                    GestureDetector(
                      onTap: _goSettings,
                      child: Image.asset('assets/images/settings-icon.png',
                          width:50),
                    ),
                  ]
              )
          ),
        )
    );
  }

  Widget _buildTableCalendar() {
    return TableCalendar(
      calendarController: _calendarController,
      events: _events,
      holidays: _holidays,
      startingDayOfWeek: StartingDayOfWeek.monday,
      calendarStyle: CalendarStyle(
        selectedColor: Colors.deepOrange[400],
        todayColor: Colors.deepOrange[200],
        markersColor: Colors.brown[700],
        outsideDaysVisible: false,
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
        TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Colors.deepOrange[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
      onDayLongPressed: _onDayLongPressed,
    );
  }

  void _onDaySelected(DateTime day, List events, List holidays) {
    setState(() {
      _actualState = AppState.createEvent;
      _actualDay = day.millisecondsSinceEpoch;
    });
  }

  void _deleteEvent(int id) async{
    DBHelper().deleteEvent(id).then((value)=>{
        _goCalendar()
    });
  }

  void _onDayLongPressed(DateTime day, List events, List holidays) {
    DBHelper().getEventsByDay(day.millisecondsSinceEpoch).then((events){
      List<Widget> children = [];
      events.forEach((key, value) {
        children.add(Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children:<Widget>[
              Text(value.getEventStringDescription()),
              GestureDetector(
                child: Image.asset('assets/images/delete-cross.png',
                    width:20),
                onTap: (){_deleteEvent(key);},
              )
            ]
        )
        );
      });
      showMaterialResponsiveDialog(
        context: context,
        title: "Studi Salvati",
        child: Center(
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: Column(
              children: children
            ),
          ),
        ),
      );
    });
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {

  }

  void _onCalendarCreated(DateTime first, DateTime last, CalendarFormat format) {

  }

  void _goCalendar(){
    _eventToCreate = [];
    updateEvents();
    setState(() {
      _actualState = AppState.calendar;
    });
  }
  void _goTuner(){
    setState(() {
      _actualState = AppState.tuner;
    });
  }
  void _goTimeKeeper(){
    setState(() {
      _actualState = AppState.timeKeeper;
    });
  }
  void _goStats(){
    setState(() {
      _actualState = AppState.statistics;
    });
  }
  void _goSettings(){
    setState(() {
      _actualState = AppState.settings;
    });
  }
  void _goArguments(){
    updateArgs();
    setState(() {
      _actualState = AppState.argumentsList;
    });
  }
}
