class EventItem{
  String arg = "";
  String durationString = "";
  int duration = 0;
  int datetime = 0;

  EventItem();

  EventItem.fromData(String arg,String dString,int d,int datetime){
    this.arg = arg;
    this.durationString = dString;
    this.duration = d;
    this.datetime = datetime;
  }

  String getEventStringDescription(){
    return this.durationString + " di " + this.arg;
  }

  void setDuration(String durString) {
    durationString = durString;
    if (durString == "15 minuti") {
      duration = 15;
    } else if (durString == "30 minuti") {
      duration = 30;
    } else if (durString == "45 minuti") {
      duration = 45;
    } else if (durString == "1 ora") {
      duration = 60;
    } else if (durString == "1.5 ore") {
      duration = 90;
    } else if (durString == "2 ore") {
      duration = 120;
    }
  }

  bool isReadyToSave(){
    if(this.arg.length>0 && this.duration>0 && this.datetime>0){
      return true;
    }
    return false;
  }

  DateTime getDateTime(){
    return DateTime.fromMillisecondsSinceEpoch(this.datetime);
  }

  @override
  String toString(){
    return this.arg+" "+this.durationString+" "+this.datetime.toString();
  }
}